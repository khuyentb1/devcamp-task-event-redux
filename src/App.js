import Task from './components/task/Task';

function App() {
  return (
    <div className="App">
      <Task />
    </div>
  );
}

export default App;
